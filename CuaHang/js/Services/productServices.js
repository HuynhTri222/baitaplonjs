const axiosClient = axios.create({
    baseURL: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/",
});

function getProductsList() {
    return axiosClient({
        method: 'GET',
        url: 'products',
    });
}

function addProducts(products){
    return axiosClient({
        method:'POST',
        url:'products',
        data: products,
    });
}

function deleteProducts(id){
    return axiosClient({
        method:'DELETE',
        url:`products/${id}`,
    });
}


function getDetailProduct(id){
    return axiosClient({
        method:'GET',
        url:`products/${id}`,
    });
}

function updateProducts(id,products){
    return axiosClient({
        method:'PUT',
        url:`products/${id}`,
        data: products,
    });
}
