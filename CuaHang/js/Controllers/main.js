
var productList = [];
var productsCartList = [];

function isID(product,id) {
    return product.id === id;
};
    

//ADD product into cart
function addProductIntoCart(i) {
    var index = productsCartList.findIndex(function (product) {
        return product.id === i;
    });
    console.log(index);
    if (index === -1) {
        //const element = productList.find( ({ id }) => id === i );
        const element = productList.find(function (product) {
            return product.id === i;
        });
        const products = new ProductCart(element.id, element.name, element.image, element.description, element.price, element.inventory, element.rating, element.type, 1);
        productsCartList.push(products);
    }
    else{
        productsCartList[index].quantity++;
    }
    
    renderCart();
    saveData();
}

function setGetProductList() {
    getProductsList().then(function (response) {
        productList = response.data;
        renderHTML();
    });
}

const renderHTML = function (arr) {
    arr = arr || productList;
    var htmlContent = "";
    for (let index = 0; index < arr.length; index++) {
        const element = arr[index];
        htmlContent += `<div class="phone__item my-5">
                            <div class="divImg">
                                <img src="${element.image}" alt="" width="180" height="180">
                            </div>
                            <div class="detailPhone">
                                <h6>${element.name}</h6>
                                <p>ID: ${element.id}</p>
                                <p>Description: ${element.description}</p>
                                <p>Price: <span class="text-danger font-weight-bold">${element.price}<u>đ</u></span></p>
                                <p>Inventory: ${element.inventory}</p>
                                <p>Rating: ${element.rating}</p>
                                <p>Type: ${element.type}</p>
                                <button onclick="addProductIntoCart('${element.id}')" class = "btn btn-success m-1">Add to cart</button>
                                
                            </div>
                        </div>`;
    }
    document.getElementById("phone__content").innerHTML = htmlContent;
}

const renderCart = function (arr) {
    arr = arr || productsCartList;
    var htmlContent = "";
    var tongTien = 0;
    for (let index = 0; index < arr.length; index++) {
        const element = arr[index];
        htmlContent += `<tr>
                            <td><img src="${element.image}" height=100 width=100>
                            </td>
                            <td>${element.name}</td>
                            <td>${element.price}</td>
                            <td>
                                ${element.quantity}
                                <button id="btnAdd" onclick="handleQuantityAdd('${element.id}')"><i class="fa fa-plus"></i></button>
                                <button id="btnSubtract" onclick="handleQuantitySubtract('${element.id}')"><i class = "fa fa-minus"></i></button>
                            </td>
                            <td>${element.quantity * element.price} VND</td>
                            <td>
                                <button class="btn btn-danger rounded-circle" onclick="handleDeleteProduct('${element.id}')">
                                    <i class="fa fa-trash"></i>
                            </td>
                        </tr>`;
        tongTien += element.quantity * element.price;
    }
    htmlContent += `<tr>
                        <th colspan="4" style"text-aligh:center">THÀNH TIỀN</th>
                        <td>${tongTien} VND</td>
                    </tr>`;
    document.getElementById("tbodyProductCart").innerHTML = htmlContent;
}

//Xóa sản phẩm
function handleDeleteProduct(id) {
    const index = checkId(id);
    if (index !== -1){
        productsCartList.splice(index,1)
        renderCart();
    }
    
    saveData();
}


//Sort AZ - ZA by product's name
function sortByProductName() {
    var sortValue = document.getElementById("sort").value;
    if (sortValue === "az") {
        productList.sort(function (a, b) {
            var nameA = a.name.toUpperCase();
            var nameB = b.name.toUpperCase();
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            return 0;
        });
        renderHTML();
    }
    else if (sortValue === "za") {
        productList.sort(function (a, b) {
            var nameA = a.name.toUpperCase();
            var nameB = b.name.toUpperCase();
            if (nameA < nameB) {
                return 1;
            }
            if (nameA > nameB) {
                return -1;
            }
            return 0;
        });
        renderHTML();
    }
    else if (sortValue === "ss") {
        var productSSList = [];
        for (let index = 0; index < productList.length; index++) {
            const element = productList[index];
            if (element.type === "samsung") {
                productSSList.push(element);
            }
        }
        renderHTML(productSSList);
    }
    else if (sortValue === "ip") {
        var productIPList = [];
        for (let index = 0; index < productList.length; index++) {
            const element = productList[index];
            if (element.type === "iphone") {
                productIPList.push(element);
            }
        }
        renderHTML(productIPList);
    }
}

setGetProductList();

const checkId = function (id){
    for (var i = 0 ; i < productsCartList.length; i++){
        if(productsCartList[i].id === id){
            return i;
        }
    }
    return -1;
 }

function handleQuantityAdd(id){ 
    const index = checkId(id);
    console.log(productsCartList);
    if(index !== -1){
        productsCartList[index].quantity++;
        renderCart();
    }
    saveData();
}

function handleQuantitySubtract(id){ 
    const index = checkId(id);
    console.log(productsCartList);
    if(index !== -1){
        var quant = productsCartList[index].quantity;
        if(quant > 1){
            productsCartList[index].quantity--;
        }
        else if(quant === 1){
            productsCartList.splice(index, 1);
        }
        renderCart();
    }
    saveData();
}



const saveData = function(){
    localStorage.setItem("productsCartList",JSON.stringify(productsCartList));
}

const getData = function(){
    let cartListJSON = localStorage.getItem("productsCartList");
    if(!cartListJSON) return;
    
    const cartListFromLocal = JSON.parse(cartListJSON);
    for(let i = 0; i<cartListFromLocal.length;i++){
        const currentCart = cartListFromLocal[i];
        const cart = new ProductCart(
            currentCart.id, currentCart.name,  currentCart.image,  currentCart.description,  currentCart.price,  currentCart.inventory,  currentCart.rating,  currentCart.type,  currentCart.quantity)
            
            productsCartList.push(cart);
    }
    

    renderCart();
}

document.getElementById("btnPayment").addEventListener("click",payment)


function payment(){
    productsCartList.splice(0);
        renderCart();
        saveData();
}

getData();


