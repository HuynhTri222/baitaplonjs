document.getElementById("btnThemSanPham").addEventListener("click",openAddProducts);
function openAddProducts(){
    document.getElementsByClassName("modal-footer")[0].innerHTML = `<button type="submit" class="btn btn-success" onclick ="addNewProducts()" data-dismiss="modal">Thêm</button>`;
    
    document.getElementById("id").value = "";
    document.getElementById("productName").value = "";
    document.getElementById("image").value = "";
    document.getElementById("description").value = "";
    document.getElementById("price").value = "";
    document.getElementById("inventory").value = "";
    document.getElementById("rating").value = "";
}


var productList = [];

function setGetProductList() {
    getProductsList().then(function (response) {
        productList = response.data;
        renderHTML();
    });
}

function renderHTML() {
    var htmlContent = "";
    for (let index = 0; index < productList.length; index++) {
        const element = productList[index];
        htmlContent += `<div class="phone__item my-5">
                            <div class="divImg">
                                <img src="${element.image}" alt="" width="180" height="180">
                            </div>
                            <div class="detailPhone">
                                <h6>${element.name}</h6>
                                <p>ID: ${element.id}</p>
                                <p>Description: ${element.description}</p>
                                <p>Price: <span class="text-danger font-weight-bold">${element.price}<u>đ</u></span></p>
                                <p>Inventory: ${element.inventory}</p>
                                <p>Rating: ${element.rating}</p>
                                <p>Type: ${element.type}</p>
                                <button class = "btn btn-success m-1"  data-toggle="modal"
                                data-target="#myModal" data-id="${element.id}">Edit</button>
                                <button class = "btn btn-danger m-1" onclick="handleDeleteProducts(${element.id})">Delete</button>
                            </div>
                        </div>`;
    }
    document.getElementById("phone__content").innerHTML = htmlContent;
}
setGetProductList();

//Thêm sản phẩm mới
function addNewProducts(){
    const id = document.getElementById('id').value;
    const name = document.getElementById('productName').value;
    const image = document.getElementById('image').value;
    const description = document.getElementById('description').value;
    const price = document.getElementById('price').value;
    const inventory = document.getElementById('inventory').value;
    const rating = document.getElementById('rating').value;
    const type = document.getElementById('type').value;

    const products = new Product(id,name,image,description,price,inventory,rating,type);
    console.log(products.id);
    addProducts(products)
    .then(function(result1){
        handleNewProducts();
    });
}
function handleNewProducts(){
    //Service
    getProductsList().then(function(result){
        productList = result.data;
        renderHTML();
    })
}
//handleNewProducts();

//Xóa sản phẩm
function handleDeleteProducts(id){
    deleteProducts(id).then(function(){
        setGetProductList();
    });
}

//nhấn edit hiển thị thông tin sản phẩm
document.getElementById("phone__content").addEventListener("click",handeClickEdit);

function handeClickEdit(event){
    const selected = event.target;
    const id = selected.getAttribute("data-id");
    if(id){
        document.getElementsByClassName("modal-footer")[0].innerHTML = `
    <button class="btn btn-success" onclick ="handleEditProducts(${id})" data-dismiss="modal">Sửa</button>
`;   
    getDetailProduct(id).then(function(result){
        const products = result.data   
        document.getElementById('id').value = products.id;
        document.getElementById('productName').value = products.name
        document.getElementById('image').value = products.image
        document.getElementById('description').value = products.description
        document.getElementById('price').value = products.price
        document.getElementById('inventory').value = products.inventory
        document.getElementById('rating').value = products.rating
        document.getElementById('type').value = products.type
    }); 
    }
}

function handleEditProducts(id){
    // const id = document.getElementById('id').value;
    const name = document.getElementById('productName').value;
    const image = document.getElementById('image').value;
    const description = document.getElementById('description').value;
    const price = document.getElementById('price').value;
    const inventory = document.getElementById('inventory').value;
    const rating = document.getElementById('rating').value;
    const type = document.getElementById('type').value;
    const products = new Product(id,name,image,description,price,inventory,rating,type);

    updateProducts(id,products).then(function(){
        handleNewProducts();
    });
}